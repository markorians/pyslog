let dbJSONObj 		= null;
let DBSelector 		= null;
let btnSelectDB 	= null;
let table 			= null;
let tableSearcher 	= null;

let selectorMap = {
	'syslogRow':'tr.syslog',
	'tmplTableRow':'tr.tmpl-syslog',
	
	'tmplDbSelector':'#tmpl-db-selector',
	'tmplBtnSelectDb':'#tmpl-btn-select-db',
	'tmplTable':'#tmpl-table',
	'tmplTableSearcher':'#tmpl-table-searcher',
	
	'searchTDCX': (i) => { return 'td[class^=c'+i+']' },
	'searchTHCX': "th[class^='w3-light-blue c']",
};

let searchTDCX = (i) => selectorMap['searchTDCX'](i);

let tmpl = (s) => {
	return $( $( s ).html() );
};

// Row searching function by TEXT
function searchRow( txt ){
	let rows = $( selectorMap['syslogRow'] );
	let rS = 0, rH = 0;
	rows.each( ( idx, row ) => {
		let rowText = $(row).children().text();
			if( rowText.match( txt ) !== null ){
				$(row).show(); rS++;
			}
			else{
				$(row).hide(); rH++;
			}
	});
	return { 'show': rS, 'hide': rH };
}

// Row sorting function by COLUMN INDEX
// DESC sorting by default, else ASC
function sortRow( idxColumn, order ){
	if( typeof order === 'undefined' )
		order = false;
	if( dbJSONObj ){
		let ord = (
			order == true ||
			order == 1 ||
			(
				typeof order === 'string' &&
				order.toLowerCase() == 'asc'
			)
		)
		dbJSONObj.sort( ( a, b ) => {
			if( a[idxColumn] < b[idxColumn] )
				return ( ord )?-1:1;
			else if( a[idxColumn] > b[idxColumn] )
				return ( ord )?1:-1;
			else
				return 0;
		});
	}
	drawTable( dbJSONObj, table );
}

// Draw table function, request Database JSON Object and JQuery Table Object
function drawTable( db, tbl ){
	tbl.find( selectorMap['syslogRow'] ).remove();
	tbl.find( selectorMap['tmplTableRow'] ).remove();
	if( db ){
		let tableRow  = null;
		db.forEach( (row) => {
			tableRow = tmpl( selectorMap['tmplTable'] ).find( selectorMap['tmplTableRow'] ).get(0);
			row.forEach( ( column, idx ) => {
				let node = tableRow.querySelector( '.c' + idx );
				node.innerText = column;
			});
			tableRow.classList.remove('w3-hide');
			tableRow.classList.remove('tmpl-syslog');
			tableRow.classList.add('syslog');
			tbl.find('tbody').append( tableRow );
		});
	}
}

$(document).ready( ()=>{
	
	DBSelector 		= tmpl( selectorMap['tmplDbSelector'] );
	btnSelectDB 	= tmpl( selectorMap['tmplBtnSelectDb'] );
	table 			= tmpl( selectorMap['tmplTable'] );
	tableSearcher 	= tmpl( selectorMap['tmplTableSearcher'] );
	
	// Fetch DBs and append to <select>
	fetch( location.origin + '/db/' )
	.then( response => response.json() )
	.then( dbs => {
		dbs.forEach( db => {
			DBSelector.children().append(
				$('<option value="' + db + '">' + db + '</option>')
			);
	});

	// On click button DB Selector, load Sysylog data and drawTable
	btnSelectDB.children().click( () => {
		let selected = DBSelector.children().val();
		if( selected != "-" ){
			fetch( location.origin + '/db/' + selected )
			.then( response => response.json() )
			.then( syslog => {
				dbJSONObj = [];
				for( idx in syslog )
					dbJSONObj.push( syslog[idx] );
				drawTable( dbJSONObj, table );
				let finded = searchRow('');
				tableSearcher.last().text(`Show ${finded.show} of ${finded.show+finded.hide}`);
			});
		}
	});

	// Live search through text input
	tableSearcher.keyup( (ev)=>{
		let txt = $( ev.target ).val();
		let finded = searchRow(txt);
		tableSearcher.last().text(`Show ${finded.show} of ${finded.show+finded.hide}`);
	});

	// Append button after live search
	btnSelectDB.append( tableSearcher );

	// Enable row sorting by clicking on TH
	table.find( selectorMap['searchTHCX'] ).click( (ev) => {
		let findClsNum = /c(\d+)$/gm;
		let column     = $( ev.target );
		let classes    = column.attr('class');
		let arrowUp    = column.find('.arrow-up');
		let arrowDwn   = column.find('.arrow-dwn');

		let match      = findClsNum.exec( classes );
		let idxColumn  = parseInt( match[1] );

		if( arrowUp.hasClass('w3-hide') && arrowDwn.hasClass('w3-hide') ){
			// Never sorted
			sortRow( idxColumn, 'ASC' );
			arrowUp.toggleClass('w3-hide');
			$( searchTDCX(idxColumn) ).toggleClass('w3-yellow');
		}
		else{
			// already sorted
			if( !arrowDwn.hasClass('w3-hide') ){
				// disable all sorting
				arrowDwn.toggleClass('w3-hide');
				btnSelectDB.children().click();
			}
			else{
				// toggle sorting
				sortRow( idxColumn, 'DESC' );
				arrowUp.toggleClass('w3-hide');
				arrowDwn.toggleClass('w3-hide');
				$( searchTDCX(idxColumn) ).toggleClass('w3-yellow');
			}
		}
	});

	$('.body')
		.append(DBSelector)
		.append(btnSelectDB)
		.append(table);
	});
});
