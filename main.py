#!/usr/bin/env python

# Check OS Platform
PLATFORM = ''
try:
    import ctypes
    PLATFORM = 'WIN'

    if not hasattr(ctypes,'windll'):
        raise Exception

    import sys
    sys.path.append(".")
    sys.path.append("..")
    def isAdmin():
        return ctypes.windll.shell32.IsUserAnAdmin() != 0
except:
    PLATFORM = 'NIX'
    def isAdmin():
        return os.getuid() == 0

print( 'Platform : %s' % (PLATFORM) )

import os, json, Database, Syslog, WebServer
from datetime import datetime

def now():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# Init DB array
DBs          = {}

# Init net params
SyslogAddr = '0.0.0.0'
SyslogPort = 514
WebSerAddr = '0.0.0.0'
WebSerPort = 443

# init servers
SyslogServer = None
WebSrv       = None

print( 'Running with high privileges ? %s' % ( 'Yes' if isAdmin() else 'No' ) )

if not isAdmin():
	SyslogPort += 10**4
	WebSerPort += 10**4

# Init Syslog Server
SyslogServer = Syslog.UDPSyslog( SyslogAddr, SyslogPort )
print( 'Serving Syslog server at %s:%s' % (SyslogAddr, SyslogPort) )

# Init HTTPS Web Server
WebSrv = WebServer.HTTPS( WebSerAddr, WebSerPort )
WebSrv.start()
print( 'Serving Web server at %s:%s' % (WebSerAddr, WebSerPort) )


# Main loop
while True:
	msg, host = SyslogServer.recvfrom()

	try:
		syslog = Syslog.SyslogParser( msg )
	except:
		print( 'Parsing Error : %s' % (msg) )

	if not hasattr( DBs, host[0] ):
		DBs[host[0]] = Database.DB( 'dbs/' + host[0] + '.db' )
		DBs[host[0]].initDatabase()

	VALUES = (
		now(),
		host[0],
		host[1],
		msg,
		syslog.parsed['priority'],
		syslog.parsed['facility'],
		syslog.facility[ syslog.parsed['facility'] ],
		syslog.parsed['severity'],
		syslog.severity[ syslog.parsed['severity'] ],
		syslog.parsed['version'],
		syslog.parsed['hostname'],
		syslog.parsed['application'],
		syslog.parsed['processID'],
		syslog.parsed['message'],
	)

	DBs[host[0]].execute( "INSERT INTO logs VALUES ( ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,? )", VALUES )

	print( json.dumps(syslog.parsed, indent = 3) )
