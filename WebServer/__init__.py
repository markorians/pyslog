from . import Server

def HTTPS( addr='localhost', port=10443 ):
	return Server.HTTPSServer( addr, port )
