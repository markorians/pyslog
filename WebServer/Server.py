if 'HTTPServer' not in dir():
    from http.server import HTTPServer as HS
if 'BaseHTTPRequestHandler' not in dir():
    from http.server import BaseHTTPRequestHandler
if 'ssl' not in dir():
    import ssl as S
if 'threading' not in dir():
    import threading as TH
if 'os' not in dir():
    import os
if 're' not in dir():
    import re
if 'json' not in dir():
    import json
if 'sqlite3' not in dir():
    import sqlite3
if 'uuid4' not in dir():
    from uuid import uuid4
if 'traceback' not in dir():
	import traceback

class HTTPSRequestHandler( BaseHTTPRequestHandler ):
	server_version = 'PySlog'
	sys_version    = 'Server/1.0'
	WWW_ROOT       = 'www_root/'
	DEBUG          = False
	seed           = str( uuid4() )
	query          = {}

	def _set_headers( self, type='text/html; charset=utf-8', code=200 ):
		self.send_response( code )
		self.send_header( 'Content-type', type )
		self.send_header( 'Access-Control-Allow-Origin', '*' )
		self.end_headers()

	def debug( self, activate=True ):
		self.DEBUG = activate

	def log_message(self, format, *args):
		if self.DEBUG:
			print( format % args )
		try:
			log = open( 'logs/' + self.seed + '.log', 'a' )
			log.write( format % args )
			if len( self.query ) >= 1:
				log.write( '\n' )
				log.write( json.dumps(self.query, indent = 3) )
			log.write( '\n' )
			log.close()
		except Exception as e:
			print( format % args )
			raise e

	def do_HEAD(self):
		self._set_headers()

	def do_GET(self):
		realpath = self.path.replace( '/', self.WWW_ROOT )
		if self.path == '/' or self.path == '/index.html':
			html = b''
			try:
				html = open( self.WWW_ROOT + 'index.html', 'rb').read()
				self._set_headers()
			except :
				self._set_headers( 'text/html', 404 )
			self.wfile.write( html )
		elif re.search( '(\.js$)|(\.css$)', self.path ):
			ext = self.path[-2:]
			if ext == 'ss':
				self._set_headers( 'text/css', 200 )
			elif ext == 'js':
				self._set_headers( 'application/javascript', 200 )
			else:
				self._set_headers( 'text/html; charset=utf-8', 404 )
				raise Exception
			try:
				html = open( realpath, 'rb').read()
				self.wfile.write( html )
			except Exception as e:
				raise e
		elif self.path == '/favicon.ico':
			self._set_headers( 'image/x-icon', 404 )
		elif self.path[0:4] == '/db/' and re.search( '(\/db\/)(.*)(\.db)', self.path ):
			for matches in re.findall( '(\??&?(\w+)=(\w+))|(&(\w+))|(\?(\w+))', self.path ):
				if matches[4] != '':
					self.query[ matches[4] ] = None
				else:
					if matches[2].isnumeric():
						self.query[ matches[1] ] = int( matches[2] );
					else:
						self.query[ matches[1] ] = matches[2];
			file = 'dbs/' + re.findall( '/db/(.*?\.db)(\??.*)', self.path )[0][0]
			try:
				con = open( file, 'rb' ).close()
				con = sqlite3.connect( file )
				cur = con.cursor()
				if 'len' in self.query and self.query['len'] > 0 :
					cur.execute( 'SELECT * FROM logs ORDER BY rowid DESC LIMIT ?', (self.query['len'],) )
				else:
					cur.execute( 'SELECT * FROM logs ORDER BY rowid DESC LIMIT 1000' )
				con.commit()
				res = cur.fetchall()
				self._set_headers( 'application/json', 200 )
				self.wfile.write( bytearray( json.dumps( res, indent = 3 ), 'UTF-8' ) )
			except Exception as e:
				print(e)
				self._set_headers( 'application/json', 200 )
				self.wfile.write( bytearray( traceback.format_exc(), 'UTF-8' ) )

		elif self.path[0:4] == '/db/':
			self._set_headers( 'application/json' )
			try:
				self.wfile.write( bytearray( json.dumps( os.listdir('dbs'), indent = 3 ), 'UTF-8' ) )
			except:
				self.wfile.write( bytearray( '[ ]', 'UTF-8' ) )
		else:
			self._set_headers( 'text/html', 404 )

	def do_POST(self):
		self._set_headers()
		self.wfile.write('Not Implemented yet.')

class HTTPSServer( object ):
	httpd = None

	def __init__( self, addr='0.0.0.0', port=10443 ):
		try:
			self.httpd = HS( ( addr, port ), HTTPSRequestHandler )
			self.httpd.socket = S.wrap_socket(
				self.httpd.socket,
				keyfile='certs/web.key.pem',
				certfile='certs/web.crt.pem',
				server_side=True
			)
		except Exception as e:
			raise e

	def start( self ):
		TH.Thread( target=self.httpd.serve_forever ).start()

	def stop( self ):
		self.httpd.shutdown()
