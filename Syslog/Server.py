if 'socket' not in dir():
	import socket

class UDPServer( object ):
	UDPSocket = None
	lastBuffer = None
	def __init__( self, addr='0.0.0.0', port=10514 ):
		self.UDPSocket = socket.socket( family=socket.AF_INET, type=socket.SOCK_DGRAM )
		self.UDPSocket.bind( ( addr, port ) )

	def recvfrom( self, bufferSize=1024 ):
		msg, host = self.UDPSocket.recvfrom( bufferSize )
		msg = msg.decode("utf-8")
		self.lastBuffer = ( msg, host )
		return self.lastBuffer
