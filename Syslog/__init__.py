from . import Server, Parser

def UDPSyslog( addr='localhost', port=10514 ):
	return Server.UDPServer( addr, port )

def SyslogParser( msg='' ):
	return Parser.Syslog( msg )
