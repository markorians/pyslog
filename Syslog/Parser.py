if 're' not in dir():
	import re
if 'calendar' not in dir():
	import calendar
if 'date' not in dir():
	from datetime import date

class Syslog( object ):
	msg      = False
	facility = {
		0  : 'Kernel Messages',
		1  : 'User-Level Messages',
		2  : 'Mail System',
		3  : 'System Daemons',
		4  : 'Security/Authorization Messages',
		5  : 'Messages Generated Internally by Syslogd',
		6  : 'Line Printer Subsystem',
		7  : 'Network News Subsystem',
		8  : 'UUCP Subsystem',
		9  : 'Clock Daemon',
		10 : 'Security/Authorization Messages',
		11 : 'FTP Daemon',
		12 : 'NTP Subsystem',
		13 : 'Log Audit',
		14 : 'Log Alert',
		15 : 'Clock Daemon (note 2)',
		16 : 'Local Use 0  (local0)',
		17 : 'Local Use 1  (local1)',
		18 : 'Local Use 2  (local2)',
		19 : 'Local Use 3  (local3)',
		20 : 'Local Use 4  (local4)',
		21 : 'Local Use 5  (local5)',
		22 : 'Local Use 6  (local6)',
		23 : 'Local Use 7  (local7)',
	}
	severity = {
		0 : 'Emergency',
		1 : 'Alert',
		2 : 'Critical',
		3 : 'Error',
		4 : 'Warning',
		5 : 'Notice',
		6 : 'Informational',
		7 : 'Debug',
	}
	parsed   = {
		'priority'    : False,
		'facility'    : False,
		'severity'    : False,
		'version'     : False,
		'timestamp'   : False,
		'hostname'    : False,
		'application' : False,
		'processID'   : False,
		'message'     : False,
	}
	reverseMonth = {month: index for index, month in enumerate(calendar.month_abbr) if month}

	def __init__( self, msg=False ):
		self.msg = msg
		if msg:
			self.parse( msg )

	def parse( self, new=False ):
		if new is not False:
			self.msg = new

		splitted = re.split( '\s|:', self.msg )
		words = []
		nums  = []
		month = ''

		for split in splitted:
			if split != '':
				if split.isnumeric() and len(nums) < 5:
					nums.append( int(split) )
				elif re.search( '(<(\d+)>(\d+)( )([a-zA-Z]{3}))|(<(\d+)>([a-zA-Z]{3}))', split ) and len(nums) < 5:
					groups = re.findall( '(<(\d+)>(\d+)( )([a-zA-Z]{3}))|(<(\d+)>([a-zA-Z]{3}))', split )
					if groups[0][0] == '':
						nums.append( int( groups[0][6] ) ) # Priority
						month = groups[0][7]
					else:
						nums.append( int( groups[0][1] ) ) # Priority
						self.parsed['version'] = int( groups[0][2] )
						month = groups[0][4]

				else:
					words.append( split )

		# Priority ever first number
		self.parsed['priority'] = nums.pop(0)
		self.parsed['facility'] = self.parsed['priority'] >> 3
		self.parsed['severity'] = self.parsed['priority'] & 0x07

		# Now, check numbers, and extract date and time
		day       = str( nums.pop(0) )
		month     = str( self.reverseMonth[month] ) if self.reverseMonth[month] > 9 else '0' + str( self.reverseMonth[month] )
		year      = str( date.today().year )
		timearray = [ str(n) if int(n)>9 else '0'+str(n) for n in nums ][0:3]
		time      = ':'.join( timearray )
		finaldate  = '-'.join( [ year, month, day] ) + ' ' + time

		self.parsed['timestamp'] = finaldate

		for word in words:
			if re.search( '([a-zA-Z]+)\[(\d+)\]', word ):
				groups = re.findall( '([a-zA-Z]+)\[(\d+)\]', word )
				self.parsed['application'] = groups[0][0]
				self.parsed['processID']   = groups[0][1]
			if re.search( '(([0-9A-Fa-f]{2}[:-]?){6})', word ):
				groups = re.findall( '(([0-9A-Fa-f]{2}[:-]?){6})', word )
				self.parsed['hostname'] = groups[0][0]

		words = [ word for word in words if not re.search( '([a-zA-Z]+)\[(\d+)\]', word ) ]

		self.parsed['message'] = ' '.join( words )

		return self.parsed
