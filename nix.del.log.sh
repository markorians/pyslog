#!/usr/bin/env bash

for d in */ ; do
	PYCACHE="${d}__pycache__";
	if [[ -d "${PYCACHE}" ]]; then
		echo "Folder [$PYCACHE] : Deleting..."
		rm -rf $PYCACHE;
	fi
	if [[ "$d" == "dbs/" ]]; then
		DBS="${d}*.db";
		echo "SQLite Databases [$DBS] : Deleting..."
		rm -rf $DBS;
	fi
	if [[ "$d" == "logs/" ]]; then
		LOG="${d}*.log";
		echo "Logs [$LOG] : Deleting..."
		rm -rf $LOG;
	fi
done
