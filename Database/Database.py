if 'sqlite3' not in dir():
    import sqlite3

class Database( object ):
	connection = None
	results    = None
	def __init__( self, filename ):
		self.results = {}
		try:
			self.connection = sqlite3.connect( filename )
			self.execute('''PRAGMA journal_mode = OFF;''')
		except Exception as e:
			connection = None
			raise e
	def execute( self, stmt, vars=None ):
		try:
			cur = self.connection.cursor()
			if vars == None:
				cur.execute( stmt )
			else:
				cur.execute( stmt, vars )
			self.connection.commit()
			r = cur.fetchall()
			self.results[ len( self.results ) ] = {
				'statement' : stmt,
				'vars' 		: vars,
				'result' 	: r,
			}
			return r
		except Exception as e:
			self.results[ len( self.results ) ] = {
				'statement' : stmt,
				'vars' 		: vars,
				'exception' : e,
			}
			raise e
	def lastResult( self ):
		return self.results
	def close( self ):
		self.connection.close()
		self.connection = None
		return self
	def isInitialized( self ):
		if self.connection is not None:
			tables = self.execute('''SELECT name FROM sqlite_master''')
			if 'logs' in tables:
				return True
			else:
				return False
		return False
	def initDatabase( self ):
		if self.connection is not None and not self.isInitialized():
			cur = self.connection.cursor()
			stmt = '''
				CREATE TABLE IF NOT EXISTS logs(
					date        VARCHAR(25),
					ip          VARCHAR(15),
					port        INTEGER,
					raw_msg     TEXT,
					priority    INTEGER,
					facility    INTEGER,
					fac_txt		VARCHAR(15),
					severity    INTEGER,
					sev_txt		VARCHAR(45),
					version     INTEGER,
					hostname    VARCHAR(25),
					application VARCHAR(25),
					processID   INTEGER,
					message     TEXT
				)
			'''
			cur.executescript( stmt )
			self.connection.commit()
			self.results[ len( self.results ) ] = {
				'statement' : stmt,
				'vars' 		: None,
				'result' 	: None,
			}
		return self
